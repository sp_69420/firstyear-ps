#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int comp_array(char array1[], char array2[], int len){
	int i;

	for (i = 0; i < len; i++) {
		if (array1[i] != array2[i]) {
			return 0;
		} 
	}
	return 1;
}

int main(){
	char num[4], revnum[4];
	int string_len;

	scanf("%s", num);

	string_len = strlen(num);

	int j = 0;
	for (int i = string_len - 1; i >= 0; i--) {
		/*printf("i = %d, j = %d\n", i, j);*/
		revnum[j] = num[i];
		/*printf("revnum = %s, num = %c\n", revnum, num[i]);*/
		j++;
	}

	if (comp_array(num, revnum, string_len)) {
		printf("Yes\n");
	} else {
		printf("No\n");
	}

	return 0;
}
