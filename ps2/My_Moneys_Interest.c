#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    float principal, rate, time;

    scanf("%f %f %f", &principal, &rate, &time);

    float simple_interest = principal * rate * time / 100.00;

    float amount = principal * (powf((1 + rate / 100.00), time));
    float compound_interest = amount - principal;

    printf("%.2f %.2f \n", simple_interest, compound_interest);

    return 0;
}
