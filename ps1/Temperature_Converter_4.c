#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */    
    int F;
    
    scanf("%d", &F);
    
    float C = ((float)F - 32) * (5/9.0);
    
    printf("%.2f\n", C);
    return 0;
}
