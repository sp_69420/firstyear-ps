#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */    
    int a, b;
    float c, d;
    
    scanf("%d %d", &a, &b);
    scanf("%f %f", &c, &d);
    
    printf("%d %d\n", a + b, abs(a - b));
    printf("%.1f %.1f\n", c + d, fabs(c - d));
    
    return 0;
}
