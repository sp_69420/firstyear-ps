#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    float basic, HRA, DA;

    scanf("%f", &basic);

    if (basic <= 10000) {
        /*printf("basic <= 10000\n");*/
        DA = (10.0 / 100.0) * basic;
        HRA = (8.0 / 100.0) * basic;
    } else if (basic >= 10000 && basic <= 20000) {
        /*printf("10000 <= basic <= 20000\n");*/
        DA = (20.0 / 100.0) * basic;
        HRA = (16.0 / 100.0) * basic;
    } else {
        /*printf("basic > 20000\n");*/
        DA = (30.0 / 100.0) * basic;
        HRA = (24.0 / 100.0) * basic;
    }
    
    /*printf("DA = %f, HRA = %f", DA, HRA);*/
    float gross_income = basic + DA + HRA;

    printf("%.2f\n", gross_income);

    return 0;
}

