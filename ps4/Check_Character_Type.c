#include <stdio.h>

int main(){
	char character;

	scanf("%c", &character);

	int char_code = (int)character;

	if (char_code >= 48 && char_code <= 57 ) {

		printf("Number");

	} else if (char_code >= 65 && char_code <= 90) {

		printf("Uppercase");

	} else if (char_code >= 97 && char_code <= 122) {

		printf("Lowercase");

	} else {

		printf("SpecialCharacter");

	}

	return 0;
}
