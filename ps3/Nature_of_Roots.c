#include <stdio.h>
#include <math.h>

int main(){
	int a, b, c;

	scanf("%d %d %d", &a, &b, &c);

	if (a == 0){
		printf("Invalid Coefficient\n");
	} else {

		int D = (int)pow((double)b, 2) - (4 * a * c);

		if (D > 0){
			printf("Roots are real and distinct\n");
		} else if (D == 0) {
			printf("Roots are real and equal\n");
		} else if (D < 0) {
			printf("Roots are imaginary\n");

	}

	
	}
	return 0;
}
